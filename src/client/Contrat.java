package client;

import java.util.HashMap;
import java.util.Map.Entry;


public class Contrat {
	private String name;
	private User client;
	private DocumentFile document;
	public Contrat(String name, User client, DocumentFile document) {
		super();
		this.name = name;
		this.client = client;
		this.document = document;
		document.open();
		document.writeTitle(name);
	}	
	public Contrat(String name, User client) {
		super();
		this.name = name;
		this.client = client;
		this.document = new Pdf(name);
		document.open();
		document.writeTitle(name);
	}
	void writeTitle(String title){
		this.document.writeTitle(title);
	
	}
	void writeSubTitle(String text){
		this.document.writeSubtitle(text);
	}
	void writeParagraph(String text){
		this.document.writeParagraph(text);
	
	}
	void done(){
		placeToSign();
		document.close();
	}
	private void placeToSign(){
		document.writeParagraph("\n\n\nFirma.\n");
		document.writeParagraph("\n--------------------------\n");
		HashMap<String,String> infoClient = client.getInfo();

for(Entry<String, String> entry : infoClient.entrySet()) {
	String camp = entry.getKey();
	String value = entry.getValue();
	document.writeParagraph(camp+": "+value+"\n");

}
	}
	
	
}
