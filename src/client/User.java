package client;

import java.util.HashMap;

public class User {
	private String name;
	private int id;
	private String adress;
	private int phone;
	public User(String name, int id, String adress, int phone) {
		this.name = name;
		this.id = id;
		this.adress = adress;
		this.phone = phone;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}

	public HashMap getInfo() {
		HashMap<String,String> info = new HashMap();
		info.put("Nombre", name);
		info.put("Cedula", Integer.toString(id));
		info.put("Correo electronico", adress);
		info.put("Telefono",  Integer.toString(phone));
		return info;
		
	}
}
