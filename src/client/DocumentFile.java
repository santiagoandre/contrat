package client;



public abstract class DocumentFile {
	private String name;
	DocumentFile(String name){
		this.name= name;
		
	}
	
	abstract void open();
	abstract void close();
	abstract void save();
	abstract  void addPage();
	abstract  void writeTitle(String s);
	abstract  void writeSubtitle(String s);
	abstract  void writeParagraph(String s);
	//abstract  void writeImage(String s);
	//abstract  void writeTable(String s);
}