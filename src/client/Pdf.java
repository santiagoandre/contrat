package client;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;


public class Pdf extends DocumentFile{
	private Document document;
	 private static Font paragraphFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
	            Font.NORMAL, BaseColor.BLACK);
	    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 20,
	            Font.BOLD);
	    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 15,
	            Font.BOLD);
	public Pdf(String name)  {
		super(name);
		FileOutputStream file;
		try {
			file = new FileOutputStream(name+".pdf");
			document = new Document();
			PdfWriter.getInstance(document,file);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	

	@Override
	void open() {
		if(document != null)
			document.open();
		
	}

	@Override
	void close() {
		if(document != null)
			document.close();
	}

	@Override
	void writeParagraph(String text) {
		Paragraph p = new Paragraph(text);
		try {
			document.add(p);
		} catch (DocumentException e) {
			
		}
	}

	@Override
	void addPage() {
		document.newPage();
	}

	@Override
	void writeTitle(String title) {
		Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(title, titleFont));

        preface.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface, 1);
      




        try {
			document.add(preface);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@Override
	void writeSubtitle(String text) {
		Paragraph preface = new Paragraph();
        preface.add(new Paragraph(text, smallBold));



        try {
			document.add(preface);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

	@Override
	void save() {
		// TODO Auto-generated method stub
		
	}
	

}
